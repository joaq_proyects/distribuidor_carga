
from wsgiref.simple_server import make_server
import json, sys
import redis

def refresh_settings():
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    # 0:HOST; 1:%
    sa = json.loads(r.get("servers_availables"))["servers"]
    # %ASSIGNABLE
    resp = []
    # Debo sumar el total
    total = 0
    for x in sa:
        total += int(x[1])
    for x in sa:
        resp.append( [x[0], (int(x[1]) / total), 0] )
    return resp


# 0:ip; 1:capacity; 2:current
settings = refresh_settings()
total_req = 0


def app(environ, start_response):
    global total_req
    global settings
    if(total_req == 0): # Agregar criterio para ejecutar que tenga sentido
        # 0:ip; 1:capacity; 2:current
        settings = refresh_settings()
        total_req = 0

    # Aumento contador de request
    total_req += 1        
    servers_len = len(settings[0])
    maxInd = 0
    for ind in range(1, servers_len):
        # capacity - (current / total)
        aux = settings[ind][1] - (settings[ind][2] / total_req)
        if(aux > settings[maxInd][1] - (settings[maxInd][2] / total_req)):
            maxInd = ind

    settings[maxInd][2] += 1
    
    # PARA DEBUG
    aux = ""
    for i in range(0, servers_len):
        aux += str(settings[i][2]) + ";"
    print(aux)

    status = "302 Found"
    response_headers = [('Location', settings[maxInd][0])]
    response_body = b"1"
    start_response(status, response_headers)
    return [response_body]


puerto = sys.argv[1] if len(sys.argv) > 1 else "9876"

print("Escuchando puerto " + puerto + "...")
httpd = make_server('',int(puerto), app).serve_forever()

