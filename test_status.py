
from wsgiref.simple_server import make_server
import json, sys
import psutil

def app(environ, start_response):

    try:
        cpu = psutil.cpu_percent(interval=1)
        # svmem(total=17121136640, available=10702954496, percent=37.5, used=6418182144, free=10702954496)
        mem = list(psutil.virtual_memory())[2]
        # sdiskusage(total=239530405888, used=80505790464, free=159024615424, percent=33.6)
        dsk = list(psutil.disk_usage('/'))[3]
        usageIndex = round(1 - ( ((100-cpu)/100) * ((100-mem)/100) * ((100-dsk)/100) ), 5)
        # print(usageIndex)

        status = "200 OK"
        response_body = json.dumps({"usageIndex":usageIndex}).encode()
    except Exception as ex:
        print(ex)
        status = "500 Internal Error"
        response_body = json.dumps({"Error":"Error interno"}).encode()

    response_headers = [
        ('Content-Type', 'application/json'),
        ('Content-Length', str(len(response_body)))
    ]
    start_response(status,response_headers)
    return [response_body]

puerto = sys.argv[1] if len(sys.argv) > 1 else "6543"

print("Escuchando puerto " + puerto + "...")
httpd = make_server('',int(puerto), app).serve_forever()

