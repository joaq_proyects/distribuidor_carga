
import redis
import json

# HOST; %CARGA BASE 100
servers_availables = json.dumps({"servers": [
    ["http://127.0.0.1:8001", 50],
    ["http://127.0.0.1:8002", 20],
    ["http://127.0.0.1:8003", 30]
]})

# DEBO SETEAR LA SYM KEY (db_msg) EN ALGUN LADO QUE PERSISTA LLAMADAS DE LA SESION
r = redis.StrictRedis(host='localhost', port=6379, db=0)
r.set("servers_availables", servers_availables)