
from wsgiref.simple_server import make_server
import json, sys

def app(environ, start_response):

    status = "200 OK"
    response_body = json.dumps({"k1":"v1"}).encode()
    
    response_headers = [
        ('Content-Type', 'application/json'),
        ('Content-Length', str(len(response_body)))
    ]
    start_response(status,response_headers)
    return [response_body]


puerto = sys.argv[1] if len(sys.argv) > 1 else "60000"

print("Escuchando puerto " + puerto + "...")
httpd = make_server('',int(puerto), app).serve_forever()

